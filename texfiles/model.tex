\chapter{Computational Fluid Dynamics}
\label{chap:model}

\section{Equations of Fluid Motion}
\label{sec:equations}
In this thesis, it is assumed that the modelled fluid is incompressible.
Specifically, the density, $\rho$ is considered to be constant.
This assumption leads to a simplification of the equations governing the motion of the fluid.
Conservation of mass implies that, in this case, the velocity field must be solenoidal,

\begin{equation} 
\label{eq:continuity}
\pdiff{u_i}{x_i} = 0.
\end{equation}
Here, $u_i$ are the Cartesian components of the velocity field and $x_i$ are the spatial coordinates, summation over repeated indices is implied.

The conservation of momentum  of a viscous fluid is described by the Navier-Stokes equations,
\begin{equation} \label{eq:N-S}
\pdiff{u_i}{t} + \pdiff{}{x_j}(u_i u_j) = -\frac{1}{\rho}\pdiff{\tilde{p}}{x_i} + \nu \frac{\partial^2 u_i}{\partial x_j \partial x_j}
\end{equation}
where $\tilde{p}$ is the pressure, and $\nu$ is the kinematic viscosity.
For brevity, the modified pressure $p=\tilde{p}/\rho$ will be further referred to as the ``pressure''.

Equations ~\eqref{eq:continuity} and~\eqref{eq:N-S} must be complemented with initial and boundary conditions.
In simulations of turbulent flow, the role of initial conditions is commonly to introduce perturbations that will facilitate the development of turbulence.
Also, for statistically steady flows, accurate initial conditions can lead to faster convergence.
In this work, uniform values of pressure and velocity are used as initial conditions, unless stated otherwise.
The perturbations introduced by the dispersive nature of the numerical schemes used for discretization (see section \ref{sec:numerics}) were found to be sufficient to trigger the development of turbulence. 

It was mentioned in the introduction that inflow boundary conditions, in particular the velocity values prescribed at the inlet, are the central topic of this thesis.
Mathematically, the boundary condition at the inlet can be classified as a ``velocity inlet'', that is,  a combination of an inhomogeneous Dirichlet condition for the velocity field and a homogeneous Neumann condition for the pressure.
Other boundary conditions that will be employed in this work are the following. At outflow  boundaries the ``pressure outlet'' condition is used, which is homogeneous Neumann for the velocity and homogeneous Dirichlet for the pressure.
At boundaries that correspond to walls the ``no-slip'' condition is applied.
Since no moving walls are considered in this work, this entails a homogeneous Dirichlet condition for velocity and a homogeneous Neumann condition for the pressure.

Some notation that will be used throughout this thesis is now introduced.
A Cartesian coordinate system $(x, y, z)$ will be employed.
The three coordinates correspond to the streamwise, wall-normal and spanwise directions, respectively.
The associated components of the velocity field are $(u, v, w)$.
Angular brackets $\langle \rangle$ will be used to denote the average value, where the averaging is performed both in time and along statistically homogeneous spatial directions. 
The fluctuating component of some quantity will be denoted by a prime, e.g. $u' = u - \langle u \rangle$ is fluctuation of the streamwise velocity.
Bold font will be used to denote vectors.


\section{Large-Eddy Simulation}
\label{sec:les}

\subsection{Scale Separation via Filtering}
In chapter \ref{chap:introduction}, it was mentioned that, in the case of turbulent flow, solving~\eqref{eq:continuity} and~\eqref{eq:N-S} on the computer is often impossible due to the computational power that this would require.
Large-eddy simulation was pointed out as an alternative approach which combines the modelling of small turbulent structures and direct simulation of larger ones.

It stems from this informal definition that at the heart of LES is a procedure that allows to perform a separation of scales.
The turbulent structures are somehow categorized into ``small'' and ``large''.
This is not trivial, because in turbulent flows energy is distributed continuously over the whole range of existing scales. 
In LES the separation is achieved by applying a filtering operation~\cite{Sagaut:1},

\begin{equation}
\label{eq:lesfilter}
\bar \phi(\textbf{x},t) =
\iiint \limits_{-\infty}^{\infty}\phi(\textbf x,t)
G \left( \textbf{x}-\bm{\xi} , \Delta\right)\text{d}^3 \bm{\xi}.
\end{equation}
Here, $\phi$ is some flow variable, $\bar{\phi}$ is its resolved part, $G$ is the kernel of the filter, and $\Delta$ is a characteristic length-scale also referred to as the filter width.
The scales that are filtered out in~\eqref{eq:lesfilter} are referred to as subgrid scales (SGS).

The definitions of $G$ and $\Delta$ dictate what turbulent motion is resolved and what is modelled.
In practice, it is common that the choice of these filter parameters is tightly connected to the discretization practices employed in the process of solving the equations of motion.
The discussion of filtering will therefore be continued in the next section, which is dedicated to numerical methods.

The filtering operation defines a new set of unknowns, namely the filtered velocity $\bar{u}_i$ and the filtered pressure $\bar{p}$.
In order to derive the equations governing these quantities, filtering is applied to~\eqref{eq:continuity} and~\eqref{eq:N-S}.
This gives the following result.

\begin{align}
\label{eq:continuityFiltered}
&\pdiff{\bar{u}_i}{x_i} = 0,\\
\label{eq:N-SFiltered}
&\pdiff{\bar u_i}{t} + \pdiff{}{x_j}(\overline{u_i u_j}) =
-\pdiff{\overline p}{x_i} + \nu \frac{\partial^2 \bar{u}_i}{\partial x_j \partial x_j}.		
\end{align}
Note that the term $\overline{u_i u_j}$ is not readily expressible in terms of the new unknowns.
By adding and subtracting $\bar{u}_i\bar{u}_j$ to $\overline{u_i u_j}$ we get, 

\begin{equation}
\label{eq:N-SFiltered2}
\pdiff{\bar u_i}{t} + \pdiff{}{x_j}(\bar u_i \bar u_j) = 
-\pdiff{\bar p}{x_i} - \pdiff{{B}_{ij}}{x_j}+\nu \frac{\partial^2 \bar{u}_i}{\partial x_j \partial x_j}, 		
\end{equation}
where 
\begin{equation}
	\label{eq:sgstensor}
	{B}_{ij} = \overline{u_i u_j} - \bar u_i \bar u_j 
\end{equation}
is the SGS stress tensor. 
Physically, it represents the dynamic coupling between the resolved and subgrid turbulent fluctuations.
The SGS stress tensor has to be modelled in order to close the system of equations.
A brief review of some of the existing approaches is given in the next section.

\subsection{Modelling the Subgrid Stresses}
\label{sec:sgs}

A rich variety of approaches to modelling $B_{ij}$ has been developed. A thorough description of many of them can be found in~\cite{Sagaut:1}.
However, only a limited number of the models proposed in the literature has found its way into  general-purpose CFD packages. 
Some models that have found widespread use are described below.  

A common approach to SGS-modelling is to employ the Boussinesq approximation, which is
the hypothesis that the subgrid stresses can be modelled in a way structurally similar to the viscous stress,~\cite{Sagaut:1}.
This boils down to adding a space- and time-dependent subgrid viscosity, $\nu_{sgs}$,  to the kinematic viscosity $\nu$ in the viscous term of the filtered Navier-Stokes equations~\eqref{eq:N-SFiltered}. 

The task is then to obtain a way of calculating $\nu_{sgs}$. 
This is possible under the hypothesis, that a characteristic length scale $l_{sgs}$ and time scale $t_{sgs}$ are sufficient to describe the subgrid scales,~\cite{Sagaut:1}.
Dimensional analysis leads to the SGS viscosity being calculated as
\begin{align}
	\nu_{sgs} \sim  \frac{l^2_{sgs}}{t_{sgs}} = u_{sgs}l_{sgs},
	\label{eq:nuprop}
\end{align}
where $u_{sgs}$ is the corresponding velocity scale.
A natural choice for $l_{sgs}$ is the filter width~$\Delta$. 
The velocity scale $u_{sgs}$ is conveniently  represented as the square root of the SGS turbulent kinetic energy $k_{sgs}$.

One popular approach is then to solve a transport equation to obtain $k_{sgs}$.
This was proposed independently by several researchers,~\cite{Horiuti1985, Kim1999,Schumann1975, Yoshizawa1982, Yoshizawa1985,Stevens1999}. 
The equation is as follows,
\begin{equation}
\label{eq:oneqeddy}
\pdiff{k_{sgs}}{t} + \pdiff{\bar{u}_i k_{sgs}}{x_i} =
2 \nu_{sgs}  \overline{D}_{ij}\overline{D}_{ij}-
C_e \frac{k_{sgs}^{3/2}}{\Delta} +
\pdiff{}{x_i}\left( \nu_{sgs} \pdiff{k_{sgs}}{x_i}\right)
+\nu \pdiff{^2 k_{sgs}}{x_i \partial x_i},
\end{equation}
where $\overline{ {D}}_{ij}$ is the filtered rate of strain tensor, and $C_e = 1.048$ is a model constant. 
The expression for $\nu_{sgs}$ is then taken to be
\begin{equation}
\label{eq:nusgs}
 \nu_{sgs} = C_k \Delta \sqrt{k_{sgs}}, 
\end{equation}
where, $C_k = 0.094$, is another model constant.

Physically, the four terms on the right-hand side of~\eqref{eq:oneqeddy} represent, respectively, the production of turbulence by the resolved scales, turbulent dissipation, turbulent diffusion, and viscous dissipation. More details on the derivation of~\eqref{eq:oneqeddy} and the employed modelling assumptions can be found on page 128 in~\cite{Sagaut:1}.
The combination of ~\eqref{eq:oneqeddy} and~\eqref{eq:nusgs} will be further referred to as the one-equation model.

Another popular approach, known as the Smagorinsky model~\cite{Smagorinsky1963}, is to use $\overline{D}_{ij}$ as the inverted time scale $1/t_{sgs}$.
This leads to
\begin{equation}
	\label{eq:smagorinsky}
	\nu_{sgs} = (C_s \Delta^2) \overline{D}_{ij},
\end{equation}
where $C_s = 0.15$ is the Smagorinsky constant.

A known downside to both the one-equation and the Smagorinsky model is that they suffer from incorrect behaviour of $\nu_{sgs}$ in the limit $y \to 0$, where $y$ is the distance from the wall.
Namely, the values of the $\nu_{sgs}$ get over-predicted.
This can be rectified by introducing a damping function.
A commonly used function is the one proposed by van Driest~\cite{Driest1956}.
It can be implemented as a modification of $\Delta$ near the wall,
\begin{equation}
	\label{eq:vandriest}
	\Delta_{vD} =  \frac{\kappa}{C_\Delta}y\left(1-e^{-y^+/A^+} \right).
\end{equation}
Here, $\kappa=0.41$, is the von Karman constant, $y^+$ is the distance to the wall scaled with the viscous length-scale, $C_\Delta = 0.158$, and, $A^+=26$.

A further development of SGS modelling was the invention of so called dynamic models.
These models addressed the issue of the model constants, such as $C_s$, actually  being flow dependent.
Instead of demanding a choice of  a global value, dynamic models treat the model constants as time- and space-dependent parameters, which can be calculated during the simulation via a certain procedure.
In the framework of the Smagorinsky model, this procedure was developed by Germano~\cite{Germano1991} and later improved by Lilly~\cite{Lilly1992}  and Meneveau~\cite{Meneveau1996}.
The way of obtaining the constants dynamically will not be presented here, instead the reader is referred to~\cite{Sagaut:1} and chapter 13 in~\cite{Pope2000}.

A drastically different approach to SGS modelling is to rely on the extra dissipation introduced by the used numerical methods instead of adding it explicitly.
This approach is known as implicit LES (ILES)~\cite{Grinstein2007}.
The study~\cite{Fureby2002} provides comparison between ILES and conventional SGS modelling.
In particular, flux limiting schemes are shown to give an error term consistent with the structure of conventional SGS models.
For the considered case studies, it is shown that ILES gives results that are at least not worse than those obtained with conventional models, such as the Smagorinsky model.
One aspect that makes ILES attractive is that it removes the need for computing $\nu_{sgs}$, which for some models such as the one-equation model, means removing an extra transport equation from the system.
This can mean a notable increase in the performance of the solver.

\section{Numerical Methods}
\label{sec:numerics}
In this section, the numerical methods used to solve~\eqref{eq:continuityFiltered} an~\eqref{eq:N-SFiltered2} are discussed.
All the simulations presented here were performed using the freely available general-purpose CFD library \OF{}.
Therefore, the methods presented below describe a part of the capabilities of this software.
For a more detailed description of the numerical algorithms behind \OF{} and their implementation the interested reader is referred to~\cite{Jasak1996},~\cite{DeVilliers2006}, and~\cite{Rusche2002}.

To discretize the governing equations, \OF{} uses the finite volume method (FVM).
The computational domain is decomposed into a large number of polyhedral cells, also called control volumes.
The continuous flow field is represented by the values of the flow variables in the centroids of the cells.
Within each cell the flow variables are considered constant and equal to the values in the centroid.
It can be shown~\cite{Ferziger2002} that storing the values in the centroid leads to it being a second-order accurate approximation of the average value across the volume of the cell.

Note that, by construction, using the FVM implies that we cannot resolve turbulent scales that are smaller than the local cell-size.
This introduces a natural length-scale that can be used to specify the filtering operation~\eqref{eq:lesfilter}.
Let $V$ be the (local) volume of the computational cell.
Then if  $\Delta$ is defined  as $V^\frac{1}{3}$ and the top-hat filter 
\begin{equation}
	\label{eq:tophat}
	G = \begin{cases}
		1/\Delta^3, & \textbf x \in V\\
		0, & \text{otherwise,}
	\end{cases}
\end{equation}
is employed, the resolved part of any flow variable is equal to the local cell-average.
As explained above, the latter is in turn approximated by the value in the centroid.
What this means is that no explicit filtering procedure needs to be performed.

Coupling the filtering with discretization is extremely common~\cite{Sagaut:1} due to the simplifications introduced by eliminating the need for explicit filtering.
Recall, for instance, that the stresses due to the filtered out turbulent motion are referred to as sub\textit{grid}, which implies a connection between filtering and the grid.
However, such a coupling means that the computational mesh is a part of the definition of  the governing equations.
Consequently, it becomes impossible to study the properties of LES independently of the used discretization procedure.
For instance, it is impossible to show grid independence, since, strictly speaking, changing the grid means solving a different set of equations.

In order to discretize the governing equations using the FVM it is necessary to obtain the values of the convective and diffusive fluxes through the faces of each computational cell~\cite{Ferziger2002}.
Since the values of the flow variables are only stored in the cells' centroids, the face values have to be obtained via interpolation.
The choice of interpolation schemes affects the order of accuracy of the discretization, as well as numerical stability.
To obtain reliable results, maintaining at least second-order accuracy is highly desirable.
In this work, linear interpolation was used to compute both the convective and diffusive fluxes.

Time integration was performed using a second-order accurate implicit backward-differencing scheme, see~\cite{Jasak1996} for details.

The solution of the governing equations involves two steps which form the PISO\footnote{PISO: Pressure Implicit with Splitting Operator.} pressure-velocity coupling algorithm~\cite{Issa1986, Ferziger2002}.
First, the momentum equation~\eqref{eq:N-SFiltered2} is solved in order to obtain the velocity field.
This field is, however, not necessarily divergence free.
The second step is solving a Poisson-type equation for the pressure.
The obtained pressure values are then used to correct the velocity field and enforce incompressibility.
Commonly, the second step needs to be repeated multiple times to ensure convergence.
In the simulations presented here, 3-4 repetitions were used depending on the flow case.
