\chapter{Inflow Generation}
\label{chap:inflow}
This chapter is dedicated to the discussion of inflow generation.
The first section gives an overview focusing on existing precursor-based approaches.
In the second section a new framework for generating inflow for a TBL simulation is presented.

\section{Review of Existing Approaches}
As was briefly mentioned in chapter~\ref{chap:introduction}, it is possible to distinguish two main approaches to generating turbulent inflow.
One approach is based on using another simulation, a \textit{precursor}.
The methods that fall into this category are called precursor simulation methods~\cite{Tabor2010} or recycling methods~\cite{Wu2017, Lund1998}.

The other class of methods is based on using a stochastic process.
Commonly, this involves imposing some a priori known values of one- and two-point statistics of the generated velocity fields. 
The methods based on this approach are referred to as synthesis or synthetic methods~\cite{Tabor2010, Wu2017, Jarrin2008}.

In the present study the main object of investigation is a precursor simulation based method, which is presented in the next section.
Thus, only methods of this type will be reviewed here.
The reader interested in a substantial review of synthesis methods is referred to~\cite{Tabor2010, Wu2017, Jarrin2008, Sagaut:1}.
 
Note that the idea of using an auxiliary simulation to generate the inflow only makes sense if the auxiliary simulation itself does not require an inflow generation procedure.
A class of flows that fall into this category are those with periodicity in the streamwise direction.
In particular, fully-developed turbulent channel and pipe flows have received a lot of attention.

It is therefore most straightforward to apply a precursor-based method to a simulation for which it makes sense to prescribe inlet velocity values taken directly from some periodic flow.
In that case, the velocity field from the precursor simulation is sampled from a plane perpendicular to the flow direction and directly mapped to the inlet of the main simulation.
This way of prescribing the inflow is referred to as \textit{strong recycling}~\cite{Wu2017}.
 
An example of such a configuration is sketched in figure~\ref{fig:precdirect} which illustrates the application of turbulent pipe flow for generating inflow for a simulation of an axisymmetric turbulent jet.
A precursor channel flow has been used similarly, for example, for simulating a plane diffuser~\cite{Herbst2007}.

\begin{figure}
	\centering
	\includegraphics[scale=1]{figures/precdirect}
	\caption{Schematic representation of using velocity values from a precursor simulation of periodic pipe flow as inflow for a simulation of an axisymmetric turbulent jet.}
	\label{fig:precdirect}
\end{figure}

In spite of the conceptual simplicity of strong recycling, several aspects of this procedure are not yet fully understood.
One is the relationship between the grids in the precursor and at the inlet of the main simulation.
Ideally, the grids should be identical since in that case no interpolation is needed to map the data from the precursor to the main simulation, and the turbulent inflow only contains structures that are resolvable on the mesh used at the inlet.
However, reusing a precursor for multiple simulations using different grids would be highly beneficial.
Also, being able to compute the precursor on a grid that is coarser than the one used at the inflow would decrease the relative cost of the precursor compared to the main simulation.
That is important, since the computational overhead is one of the main down-sides of precursor-based methods, as compared to synthetic ones.

In~\cite{Keating2004a}, the authors analyse the effects of applying a low-pass filter to the velocity fields sampled from a precursor before using them as inflow.
This roughly corresponds to using a coarser grid in the precursor.
It is found that filtering out eddies of size smaller than the integral length-scale has a negligible effect on the adaption length, which is a measure of the distance after which the flow recovers to the correct physical behaviour.
In~\cite{Sillero2011}, instead of using a periodic flow, a precursor simulation of a spatially-developing TBL using a coarser grid was used to provide inflow data for a DNS of a TBL.
The adaption length in the DNS was found to be very short.
These studies indicate that using a coarser grid in the precursor simulations is feasible.
However, to the author's best knowledge, no extensive evaluation of this possibility has been published to date.

An alternative way of bringing down the computational overhead associated with the precursor would be to develop a procedure allowing to use a small precursor database for generating inflow for a larger time-span.

The study of Chung and Sung~\cite{Chung1997} attempts to develop a method for doing that.
As a model problem, they consider fully-developed turbulent channel flow.
A precursor simulation with cyclic boundaries in the streamwise direction is used to provide inflow for a channel flow simulation with identical geometry, but employing inlet-outlet  boundaries.
The authors point out that instead of sampling the velocity fields from a given plane at each time-step (temporal sampling), it is possible to employ Taylor's frozen field hypothesis and use spatial sampling instead.
In that case, at some given time $t_s$ the velocities are obtained by sweeping a sampling plane through the domain in the direction of flow.
If $U_c$ is a characteristic velocity associated with convection by the mean velocity and $L$ is the length of the domain, spatial sampling is equivalent to time sampling over a period of $L/U_c$.
The obvious problem with this approach is that  $L/U_c$ is typically not a very large value, therefore providing a very limited amount of sampled fields.
The authors therefore propose a new method which combines spatial and temporal sampling.
First spatial sampling is applied, followed by temporal sampling over some period $\Delta t$ which is sufficiently large for the velocity values to become uncorrelated with those at the time the spatial sampling was used.
Then spatial sampling can be performed again, and so on.
The authors show that the method leads to a considerable saving of CPU time and the result is comparable to using temporal sampling only.

Another approach to saving CPU time and storage size would be to produce a precursor database spanning some limited time frame and then loop through the sampled values several times if velocity values for a larger time span are needed.
Some blending procedure would have to be applied to provide a smooth transition between the velocity values in the beginning and the end of the sampled database.
In~\cite{Larsson2009}, such a procedure is discussed within the context of concatenating results from two independent simulations of homogeneous isotropic turbulence in a periodic box.

The work of Nikitin~\cite{Nikitin2007a} analyses another problem with utilizing a stream\-wise-periodic precursor.
Using turbulent pipe flow as a model problem, the author shows that the periodic structure of the solution to the precursor problem is transferred to the main simulation thus leading to spurious, non-physical periodicity.
This behaviour is shown to be independent of the initial conditions used in the main simulation.
This effect can, however, be anticipated to be less prominent when the main simulation is not simply a spatially-developing version of the precursor~\cite{Wu2017}.

The discussion of precursor-based inflow generation methods is now continued with an overview of methods dedicated to simulations of spatially-developing TBLs.
In 1988, Spalart~\cite{Spalart1988} presented a way to simulate a turbulent boundary layer at a single Reynolds number using periodic boundary conditions in the streamwise direction.
This was made possible by choosing a coordinate system that was ``fitted'' to account for the growth of the TBL, therefore making the boundary layer thickness and the viscous length-scale independent of $x$.
In particular, the new wall-normal coordinate, $\eta$, was defined as a weighted average of $y^+$ and $y/\delta$.

The key assumptions that had to be made is that in the streamwise direction the growth of the inner and outer length-scales of the boundary layer, and also the change in the mean and variance values of velocity, is slow.
The components of velocity could then be represented as
\begin{equation}
\label{eq:spalart}
u_i(x,\eta,z,t) = \langle u_i \rangle(x, \eta) + A_i(x, \eta)u_{i, p}(x, \eta, z, t),
\end{equation}
where $A_i(x, \eta)$ are proportional to the standard deviation of the values of the velocity components, and $u_{i,p}$ represent the fluctuations.
By assumption, $\langle u_i \rangle$ and $A_i$ are slow-varying functions of $x$. 
Contrary to this, the functions $u_{i,p}$ are fast-varying.
By definition, their mean values is zero and the standard deviation values are independent of $x$. 
It is therefore possible to apply streamwise-periodic boundary conditions to $u_{i,p}$.

Expressing the Navier-Stokes and continuity equations in the new coordinate system leads to
a large amount of extra source terms.
This makes implementing Spalart's method in a general-purpose CFD code a tedious job, and is probably the reason why it didn't find wide-spread use~\cite{Wu1995, Lund1998}.
However, it is an accurate way to generate inflow corresponding to a TBL at a particular Re.

Wu et al~\cite{Wu1995} used the ideas of Spalart~\cite{Spalart1988} presented above  to develop a method which is suitable for a conventional simulation of a spatially-developing TBL which does not require the introduction of special terms in to the governing equations.

The key idea of the method is to use the velocity values from a plane located downstream of the inlet in order to obtain the inflow.
The plane is commonly referred to as the recycling plane and in the review~\cite{Wu2017} methods based on this idea are called \textit{weak recycling} methods.

The authors of~\cite{Wu1995} propose that at each time-step the velocity field at the recycling plane is decomposed according to~\eqref{eq:spalart}.
The similarity coordinate $\eta$ is defined as in~\cite{Spalart1988}.
The ``periodic'' part of the signal, $u_{i, p}$, is then copied to the inflow plane.
Under the assumption of slow change of the mean and standard deviation values of $u_i$ with $x$, the derivatives of these statistical quantities with respect to $x$ can be considered constant.
This provides a way to evaluate them at the inlet.
For the mean velocity components it follows that
\begin{equation}
\label{eq:wumean}
\frac{\langle u_i \rangle (x_\text{mid}, \eta, t)  - \langle u_i \rangle(x_\text{in}, \eta, t+\Delta t) }{\langle u_i \rangle (x_\text{rec}, \eta, t)  - \langle u_i \rangle(x_\text{in}, \eta, t+\Delta t) } = \frac{x_\text{mid} - x_\text{in}}{x_\text{rec} - x_\text{in}},
\end{equation}
where the index 'mid' refers to the location half way between the inlet and the recycling plane, the index 'in' refers to the inlet, and the index 'rec' refers to the recycling plane.
A similar relationship can be written out for the values of the standard deviations.
Together with the periodic part $u_{i,p}$ this gives all the components to reconstruct the signal at the inflow according to~\eqref{eq:spalart}.

The same authors, Lund, Wu, and Squires, later introduced another weak recycling method~\cite{Lund1998}, sometimes referred to as the LWS method in the literature.
Here, the velocity signal is instead decomposed using Reynolds decomposition into a mean and fluctuating part.
The coordinate $\eta$ is not employed.
Instead, the velocity values themselves are scaled using inner and outer coordinates to produce two separate profiles.

The rescaling for the mean streamwise velocity is inferred from equations~\eqref{eq:tblinner} and~\eqref{eq:tblouter}.
Since these equations are valid for any $x$ it follows directly that
\begin{align}
   &  \langle u \rangle^\text{inner}_\text{in}(y^+_\text{in}) =
   \gamma \langle u \rangle^\text{inner}_\text{rec}(y^+_\text{in}),\\
   &  \langle u \rangle^\text{outer}_\text{in}(\eta_\text{in}) =
   \gamma \langle u \rangle^\text{outer}_\text{rec}(\eta_\text{in}) + (1 -\gamma) U_{0},
\end{align}
where $\gamma$ is the ratio of friction velocities, $u_{\tau, \text{in}}/u_{\tau, \text{rec}}$, and $\eta = y/\delta$ is the outer coordinate.
The mean wall-normal velocity values are treated in a more  ad hoc manner,
\begin{align}
   &  \langle v \rangle^\text{inner}_\text{in}(y^+_\text{in}) =
   \langle v \rangle^\text{inner}_\text{rec}(y^+_\text{in}),\\
   &  \langle v \rangle^\text{outer}_\text{in}(\eta_\text{in}) =
   \langle v \rangle^\text{outer}_\text{rec}(\eta_\text{in}).
\end{align}
This is motivated by the relative unimportance of the wall-normal component compared to the streamwise.
The fluctuations are assumed to scale with $u_\tau$ throughout the whole TBL,
\begin{align}
   & (u'_i)^\text{inner}_\text{in}(y^+_\text{in}) =
   \gamma (u'_i)^\text{inner}_\text{rec}(y^+_\text{in}),\\
   & (u'_i)^\text{outer}_\text{in}(\eta_\text{in}) =
   \gamma (u'_i)^\text{outer}_\text{rec}(\eta_\text{in}).
\end{align}
The inner and outer profiles are blended using a weighting function.

The LWS weak recycling method is widely used and has been extended to work with compressible flows, environmental flows and flows with surface roughness, see~\cite{Wu2017} for references.

In spite of the success of weak recycling methods, they suffer from  some inherent down-sides.
One issue that has received significant attention is the choice of the location for the recycling plane.
The plane should be placed sufficiently far away for the distance to be larger than the size of the largest coherent structures present in the TBL.
Otherwise, there is a risk of introducing non-physical periodic forcing.
In~\cite{Simens2009} the authors place the recycling plane as far as $850 \theta_\text{in}$ to avoid this problem.
To rectify this issue, in~\cite{Spalart2006} a shift of the velocity values in the spanwise direction was added to the rescaling procedure, and in~\cite{Jewkes2011} the velocity values are instead mirrored.

The weak coupling mechanism can also give rise to problems in the beginning of the simulation, since the non-physical values at the recycling plane get copied to the inlet and therefore remain inside the domain.
In~\cite{Liu2006} this issue was addressed by making the placement of the recycling plane dynamic.

What is also important is that while the rescaling procedure shrinks the turbulent structures in the wall-normal direction to account for the growth of the TBL, the spanwise lengths remain unchanged.
This means that the turbulent structures are distorted via this ``compression'' in $y$, and the inflow is no longer a solution to the LES equations~\eqref{eq:continuityFiltered} and~\eqref{eq:N-SFiltered2}.
The distortion is proportional to the distance to the recycling plane.

\section{Strong Recycling of Channel Flow for TBL Simulations}
\label{sec:ourmethod}
\subsection{Definition of the Method}
In this section a new framework for generating inflow conditions for a flow problem with a spatially developing TBL is described.
In chapter~\ref{chap:twoflows}, the similarities between the TBL and channel flow were discussed.
Specifically, it was shown that the mean velocity profiles are close to being identical in the inner layer.
This suggests that velocity values taken from a precursor channel flow simulation can be a good candidate for an inflow condition for a TBL.
However, several issues have to be mentioned.

Firstly, the mean wall-normal velocity is zero in fully-developed channel flow, and not so in the TBL.
Possibly  this is not a very serious issue due to the small magnitude of the wall-normal velocity as compared to the streamwise component.
Recall that a similar argument was made in~\cite{Lund1998}.

Secondly, while in the inner layer channel flow and the TBL agree well, this is no longer true in the outer layer.
This concerns both the mean streamwise velocity and the Reynolds stress tensor (the second order statistics).
In particular, for channel flow the Reynolds stresses remain non-zero even in the core of the channel, whereas for the TBL they decay to zero as the flow approaches the free stream.
 
The above discrepancies can be expected to lead to the presence of an adaption region.
The analysis of its length and how this length compares to that produced by a weak recycling method is one of the main focuses of Paper~\rom{2}.

\begin{figure}[htp!]
  \centering
  \includegraphics[width=4in]{figures/rescaling_schematic}
  \caption{Schematic picture illustrating the use of a channel flow precursor simulation
    to generate inflow conditions for the main simulation. The precursor simulation is
    set up to match the desired momentum thickness of the main simulation, as described in
    section~\ref{sec:prec}.}
  \label{fig:schematic}
\end{figure}

The proposed procedure is illustrated schematically in figure~\ref{fig:schematic}.
The parameters of the channel flow are determined from those of the main simulation.
It is assumed that the inlet of the main simulation  is rectangular,
\[
0<y<b_m, \hspace{0.5cm} 0<z<h_m,
\]
where $b_m$ denotes the width of the inlet, and $h_m$ its height.
The inflow TBL is assumed to be attached to the wall located at $y=0$.
The velocity components are prescribed at the inlet using values obtained from the precursor simulation without any additional manipulation.

As discussed in chapter~\ref{chap:twoflows}, the TBL is fully characterized by one of the Re-numbers listed in~\eqref{eq:tblre}.
Here, it is assumed that $\rey_\theta$ is provided, and thus the momentum thickness of the TBL, $\theta_\text{in}$, is known, but the proposed framework can easily be reformulated for the case when $\rey_{\delta^*}$, $\rey_{\delta_{99}}$ or $\rey_{\tau}$ is defined instead.
The full list of parameters specifying the inflow is 
\[
(b_m, h_m, U_0, \theta_\text{in}, \nu).
\]

The main task is now to formulate the most appropriate channel flow precursor simulation, to provide time-resolved inflow data for the main simulation. 
The proposed solution is to match $\rey_\theta$, so that it is identical for the precursor simulation and the inflow of the main simulation.
Momentum thickness is not commonly used as a characteristic length-scale for channel flow, but it can nevertheless be computed.
The upper limit of the integral defining $\theta$ should be set to the channel half-height $\delta$, which roughly corresponds to the boundary layer thickness.
The closest analogue to the free-stream velocity $U_0$ is then the centreline velocity $U_c$.
Hence, for channel flow the momentum thickness based Re-number is defined as
\begin{equation}
	\label{eq:channelretheta}
	\rey_\theta = \frac{U_c}{\nu}\int_0^\delta \frac{\langle u \rangle}{U_c}\left( 1 - \frac{\langle u \rangle}{U_c} \right) \mbox{d}y.
\end{equation}

To make the set-up of the precursor easier, a way of determining $\rey_b$ given $\rey_\theta$ is provided below.
The full list of precursor parameters that need to be determined therefore consists of the three quantities constituting $\rey_b$ and the length and width of the computational domain:
\[
(l_p,b_p,\delta,\olU,\nu_p).
\]

\subsection{Determination of the Precursor Parameters}
 \label{sec:prec}

A procedure in four steps to determine the parameters $(l_p,b_p,\delta,\olU,\nu_p)$ for the
precursor simulation, from the parameters $(b_m, h_m, U_0, \theta_\text{in}, \nu)$ of the main simulation, is now proposed.

\begin{description}
\item[Step 1]  The width of the channel, the kinematic viscosity, and the momentum thickness
are taken to be the same as for the main simulation, i.e. $b_p=b_m$, $\nu_p=\nu$, and $\theta_p = \theta_\text{in}$.

\item[Step 2] The bulk velocity for the precursor simulation is determined as follows.
The centreline velocity of the precursor is matched to the free-stream velocity of
the main simulation, i.e.~$U_c=U_0$.
The bulk velocity is then determined by the relation,
\begin{equation} \label{eq:uc}
\frac{U_c}{\olU} = f_1(\rey_\theta),
\end{equation}
for channel flow. 
To obtain a computable semi-empirical expression, the ansatz, 
$f_1(\rey_\theta) \approx 1 + \alpha_1 \rey_\theta^{-\beta_1}$, is fitted to the DNS data of~\cite{LM:1},
which includes channel flow simulations at five different Re-numbers.
The resulting coefficient values are given in table~\ref{tab:coeff}, and the fit to data is illustrated in figure~\ref{fig:corr}.

\begin{figure}[htp!]
  \centering
  \includegraphics{figures/correlations}
  \caption{Illustration of the functions $f_1$ and $f_2$. The full lines are given by the
    semi-empirical expressions, with coefficients from table~\ref{tab:coeff}. The DNS values
    are taken from~\cite{LM:1}.}
  \label{fig:corr}
\end{figure}

\item[Step 3] The length-scale of the precursor simulation is defined by $\delta$, which is determined from the relation,
\begin{equation} \label{eq:delta}
\frac{\delta}{\theta} = f_2(\rey_\theta),
\end{equation}
for channel flow.
To obtain a computable semi-empirical expression, the ansatz $f_2\approx \gamma_2 + \alpha_2 \rey_\theta^{\beta_2}$, is fitted to the DNS-data of~\cite{LM:1}.
The resulting coefficient values are given in table~\ref{tab:coeff}, and the fit to data
is illustrated in figure~\ref{fig:corr}.

\begin{table}[H]
  \centering
  \caption{Coefficients in the semi-empirical expression for $f_1$ and $f_2$. The coefficient
  values are obtained by a least-squares fit of the two ansatzes to the DNS-data from~\cite{LM:1}.} \label{tab:coeff}
  \begin{tabular}{c|ccc}
    $i$ & $\alpha_i$ & $\beta_i$ & $\gamma_i$ \\ \hline
    1   & 0.3427     & 0.1287    & 1.000     \\
    2   & $2.603\cdot 10^{-4}$           & 0.9834    & 11.28     \\ \hline
  \end{tabular}
\end{table}

\item[Step 4] The length of the channel flow domain is determined to be proportional
to the channel's height. In Paper \rom{1},  the analysis of two-point correlations shows that the value, $l_p=8\delta$, can be considered sufficiently large.
\end{description}

Since the precursor simulation is set up based on the requirements of the main simulation, it is natural to also do this with the grid generation. It is thus recommended that the grid on the inflow patch matches the grid on the sampling plane of the precursor simulation, and that the same time-step is used in the two simulations. 
