\chapter{Two Wall-Bounded Turbulent Flows}
\label{chap:twoflows}

Two flows play a central role in this thesis: the fully-developed turbulent channel flow and the turbulent boundary layer.
In this chapter these flows are defined, and a short analysis of the physics of the flows is given.
The main objective of the chapter is to set the stage for the discussion of inflow generation methods in chapter~\ref{chap:inflow} and the simulation results in chapter~\ref{chap:results}.
For a comprehensive discussion of wall-bounded turbulent flows see chapter~7 in~\cite{Pope2000}.

\section{Turbulent Boundary Layer}
Consider a uniform-velocity non-turbulent stream that encounters a smooth flat plate of infinite length and width, aligned with the direction of the flow.
This set-up gives rise to the most simple of boundary layer type flows: the zero-pressure-gradient turbulent boundary (ZPG-TBL) over a flat plate.

At the plate, the fluid loses momentum due to friction.
But at some distance $\delta$, referred to as the boundary layer thickness, the velocity is undisturbed by this and is equal to the velocity of the free stream $U_0$.
Gradually, more and more momentum is lost at the wall, therefore leading to the growth of $\delta$ with $x$.
The flow is thus spatially developing and statistically inhomogeneous in both the streamwise and wall-normal directions.
In the spanwise direction, however, the statistics remain unchanged.
The ZPG-TBL is therefore statistically two-dimensional.
A snapshot of the velocity magnitude taken from an LES of a ZPG-TBL is shown in figure~\ref{fig:tblflow}.
The resolved turbulent structures can be clearly seen.

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{figures/tblflow.png}
	\caption{Snapshot of the velocity magnitude, scaled with the free-stream velocity $U_0$, from an LES of a ZPG-TBL attached to a flat plate located at $y=0$. Cut-plane in the spanwise direction.}
	\label{fig:tblflow}
\end{figure}

The boundary layer thickness $\delta$ is a theoretical quantity which is impossible to measure.
Therefore, three alternative ways to define a characteristic length-scale for the boundary layer exist.

\begin{itemize}
	\item $\delta_{99}$ --- the value of $y$ where $\langle u \rangle = 0.99U_0$.
		
	\item $\theta = \int_0^\infty \frac{\langle u \rangle}{U_0}\left( 1 - \frac{\langle u \rangle}{U_0} \right) \mbox{d}y$ --- the momentum thickness, which is a measure of the loss of the momentum flux in the boundary layer.
	
	\item $\delta^* = \int_0^\infty \left( 1 - \frac{\langle u \rangle}{U_0} \right) \mbox{d}y$ --- the displacement thickness, which is a measure of the loss of the mass flux in the boundary layer.
\end{itemize}
Since one needs to measure a small velocity difference to compute $\delta_{99}$, the quantity is quite poorly conditioned.
Both $\theta$ and $\delta^*$ are, on the other hand, well conditioned. 

It is possible to form a non-dimensional quantity, the Reynolds number, based on the characteristic length and velocity scales of the flow and the kinematic viscosity $\nu$.
Using the length-scales defined above, the following Reynolds numbers are formed:

\begin{equation}
\label{eq:tblre}
\rey_x = \frac{U_0x}{\nu}, \quad \rey_{\delta_{99}} = \frac{U_0 \delta_{99}}{\nu}, \quad \rey_\theta = \frac{U_0 \theta}{\nu}, \quad \rey_{\delta^*} = \frac{U_0 \delta^*}{\nu}.
\end{equation}
Each of these Reynolds numbers fully defines the state of the boundary layer.

At the leading edge of the flat plate ($x=0$) the boundary layer is laminar and remains as such until a certain critical Reynolds number is reached from which transition to turbulence begins.
After some distance downstream of that location ($\rey_x \approx 5 \cdot 10^5$) the flow is fully transitioned to the turbulent regime.
The study of the transition process is outside of the scope of this thesis, therefore the TBL is further assumed to be fully turbulent.

Figure~\ref{fig:tblumean} shows the mean velocity profile obtained from a Direct Numerical Simulation (DNS)~\cite{Schlatter2010}, that is, a simulation where all the turbulent scales are resolved.
A thin region near the wall where the velocity gradient is large is clearly visible.
The origin of this region can be explained in the following way.
Away from the wall the extra mixing introduced by turbulence smooths out the velocity gradient, making the profile flatter.
At the same time, the no-slip boundary condition enforces both the velocity  and the turbulent stresses to be zero at the boundary.
This results in the transition to the flatter profile further away from the wall being rapid and steep.

\begin{figure}[h]
	\centering
	\includegraphics[scale=1]{figures/tblumean.png}
	\caption{The mean velocity profile of a TBL at $\rey_\theta = 4060$, DNS data by Schlatter and \"{O}rl\"{u}~\cite{Schlatter2010}.}
	\label{fig:tblumean}
\end{figure}

In the wall-normal direction, the flow can thus be considered divided into two overlapping regions: the \textit{inner layer} where the flow is independent of $\delta$ and $U_0$, and the \textit{outer layer} where the effect of $\nu$ is negligible and turbulent stresses are dominant.
The part of the profile which is part of both the inner and the outer layers is called the \textit{overlap} region.
%Before expressing this mathematically, a number of definitions are needed.

The wall shear stress, $\tau_w$, can be used to define so called \textit{viscous} scales that can be used to describe the flow in the inner layer.
The velocity scale for the inner layer can be obtained as $u_\tau = \sqrt{\tau_w/\rho}$.
It is referred to as the friction velocity.
Further, the viscous length-scale, or \textit{wall unit}, can be defined as $\delta_\nu = \nu/u_\tau$.
The wall-normal coordinate expressed in wall units is defined as $y^+=y/\delta_\nu$.
It is also referred to as the inner coordinate and can be interpreted as a local Reynolds number.
In fact, setting $y$ to $\delta$ gives the definition for $\rey_\tau = \delta u_\tau/\nu$, which can also be used to define the state of the TBL along with the Re-numbers introduced earlier in \eqref{eq:tblre}.

The analysis above implies that in the inner layer the flow can be expressed as a function of $y^+$, whereas in the outer layer a function of $y/\delta$ (the outer coordinate) should be used:
\begin{align}
\label{eq:tblinner}
& \langle u \rangle/u_\tau = \Phi_1(y^+), \\
\label{eq:tblouter}
& (U_0 - \langle u \rangle)/u_\tau = \Phi_2(y/\delta).
\end{align}
Here, $\Phi_1$ and $\Phi_2$ are some functions.
The theory can be developed further.
However, this will not be needed for the discussions in this thesis and the interested reader is therefore referred to~\cite{Pope2000}, for instance.

The multi-layered nature of the TBL is characteristic of all wall-bounded turbulent flows.
The steep velocity gradient near the wall leads to a high production of turbulence in the region.
It therefore becomes crucial to accurately resolve this region in a scale-resolving simulation, and that, in turn, is the reason why such simulations demand such a vast amount of computational resources~\cite{Chapman1979, Choi2012, Rezaeiravesh2016}. 

\section{Turbulent Channel Flow}
Channel flow is defined as a flow between two infinite parallel  plates driven by a constant mean pressure gradient.
A snapshot of the velocity magnitude taken from an LES of channel flow is shown in figure~\ref{fig:channelflow}.
The geometry of the channel is fully defined by the channel height $h=2\delta$, i.e. the distance between the plates.
\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{figures/channelflow2.png}
	\caption{Snapshot of the velocity magnitude, scaled with the bulk velocity $\overline U$, from an LES of channel flow. Cut-plane in the spanwise direction.}
	\label{fig:channelflow}
\end{figure}
Due to the infinite length and width of the plates, the velocity statistics are independent of $x$ and $z$ and vary only in the wall-normal direction.
In other words, the flow is statistically one-dimensional.

To characterise the flow, the following velocity scales can be defined.
The bulk velocity is defined as
\begin{equation}
\label{eq:ubulk}
\overline{U} = \frac{1}{h}\int_0^{h} \langle u\rangle\,\mbox{d}y.
\end{equation}
The centreline velocity, $U_c$, is defined as $\langle u(\delta) \rangle$.
As a wall bounded flow, channel flow shares the multi-layered structure of the TBL, and hence in the inner layer the friction velocity $u_\tau$ is the relevant characteristic velocity.

The velocity scales together with the kinematic viscosity of the flow $\nu$ and the length-scale defined by the height of the channel form the following Re-numbers:

\begin{align}
	\label{eq:channelre}
	\rey_b = \frac{h \overline{U}}{\nu}, \quad \rey_c = \frac{U_c \delta}{\nu}, \quad \rey_\tau = \frac{u_\tau \delta}{\nu}.
\end{align}
Each of them fully defines the flow.
As with the boundary layer, channel flow can be laminar, undergoing transition or fully turbulent.
The latter regime is obtained for \mbox{$\rey_b > 1800$}~\cite{Pope2000}.

Figure~\ref{fig:tblchannelumean} shows the mean streamwise velocity profiles for channel flow and a ZPG-TBL at $\rey_\tau \approx 1000$.
A logarithmic scale is used for the abscissa in order to enlarge the near-wall region.
It is evident that the agreement between the two flows in the inner layer is excellent, the two curves are nearly indistinguishable.
This collapse of the profiles in the inner layer was observed in an abundant amount of experimental and computational results, and not only for channel flow and the TBL, but also for pipe flow~\cite{Pope2000}.
Mathematically, this means that the  function $\Phi_1$ in \eqref{eq:tblinner} is universal.
 
\begin{figure}[h]
	\centering
	\includegraphics[scale=1]{figures/tblchannelumean.png}
	\caption{Mean velocity profiles, taken form a DNS of a TBL~\cite{Schlatter2010} and channel flow~\cite{LM:1}, both at $\rey_\tau \approx 1000$.}
	\label{fig:tblchannelumean}
\end{figure}

By contrast, in the outer layer, beyond the overlap region, the agreement between the two flows is poor.
This is not surprising, since at the edge of the boundary layer the turbulence interacts with the free stream, whereas in channel flow the dynamics are different.
