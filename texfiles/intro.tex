% -*- root: lic.tex -*-
\chapter{Introduction} % (fold)
% \pagenumbering{arabic}
\label{chap:introduction}
%]


Turbulent flows play a significant role in many branches of engineering.
Examples include the aeronautic, automotive, marine, oil and gas, and other industries, where predicting the properties of fluid flow plays a crucial role in  product development.
From an academic perspective, turbulence continues to be a physical phenomenon the properties and behaviour of which are incompletely understood.
For these reasons, the study of turbulent flows is an active area of research driven by both public institutions and private companies.

In the recent decades the role of computer simulations in the study and prediction of turbulent flows has rapidly grown.
The main cause for this is the increase in the power and availability of computational resources.
Also, programming languages have evolved, and the internet has allowed for easy collaborative development of large software packages.
Thus, robust libraries dedicated to fluid flow simulations have been created.
An example of such a library is \OF{}\footnote{www.openfoam.com}, which is used for the simulations presented in this thesis.

Currently, Computational Fluid Dynamics (CFD) is already a mature field.
However, accurate prediction of turbulent motion still remains a challenge.
Its multi-scale nature makes it often impossible to directly resolve all the turbulent motion in a simulation.
This problem is especially prolific in the case of wall-bounded flows.
For example, in~\cite{Nishikawa2012} flow around a model-scale ship is simulated using approximately a \textit{billion} grid points, but the spatial resolution is still not high enough to capture the smallest turbulent scales.
Therefore, instead of simulating turbulence directly its effects are often modelled.
A large number of turbulence models have been developed, some of them applicable to a wide range of flow cases. 

Nevertheless, the desire to simulate more complex physical phenomena, poor performance  of turbulence models in certain cases, and the ever growing availability of computational resources lead to the search of a compromise between modelling turbulent motion and resolving its scales directly.
An approach  that has found wide-spread use is Large-Eddy Simulation (LES).
It combines the modelling of small turbulent structures with direct simulation of the larger energy-containing eddies.
This simulation technique is used in the computations that are presented in this work.
It is anticipated, that the role of such scale-resolving simulations will continue to grow  rapidly in the upcoming decades~\cite{Slotnick2014}.

In a CFD simulation, the spatial domain of the studied problem is always bounded.
In engineering this may correspond to some component of a more complex device or system.
In academic research this may be a finite approximation of a theoretically infinite geometry (e.g. in the case of channel flow).
The behaviour of velocity, pressure and other flow values on the boundaries of this computational domain has to be modelled in such a way that it accurately represents the real-life behaviour of the flow.

\begin{figure}
	\centering
	\includegraphics[scale=1]{figures/tpipe}
	\caption{A sketch of a T-junction, the red dotted lines represent the boundaries of the computational domain.}
	\label{fig:tpipe}
\end{figure}

As an example, consider the T-junction of two pipes featured in figure~\ref{fig:tpipe}.
Assume that the domain is bounded by the red dotted lines.
The treatment of the flow variables at the outflow boundaries should ensure that the flow can propagate out of the computational domain.
A far more challenging problem, however, is the modelling of the inflow boundary.
For instance, consider the velocity values.
When turbulence is modelled and no turbulent fluctuations are resolved, prescribing the inflow velocity boils down to assigning the correct mean values.
They can be obtained either from experimental data or, in some cases, from known analytical expressions.
However, when a significant part of the turbulent scales is resolved, the turbulent fluctuations should be prescribed at the inflow as well.
This means that the prescribed values should change with time and contain structures that are an accurate representation of the upstream turbulent flow in question.

This issue, generation of turbulent inflow for scale-resolving simulations, is the main topic addressed in this work.
In particular, inflow turbulent boundary layers (TBLs) are focused on.
Next to a streamlined body there exists a thin boundary layer, where the fluid velocity transits from zero (relative to the body) at the wall of the body to the free-stream velocity of the surrounding fluid.
For a very extensive range of industrial applications, the boundary layer is turbulent.
Many canonical flows also include a TBL.
Examples are the flows around a backward-facing step~\cite{Le1997} and over a contoured ramp~\cite{Bentaleb2012}.
 
The existing methods for inflow generation for LES can be classified into two categories~\cite{Tabor2010, Wu2017}: precursor simulation based methods and synthesis methods.
The former rely on an auxiliary simulation, referred to as the precursor, to generate the inflow.
The latter generate the data stochastically.
Precursor-based methods offer the best accuracy~\cite{Tabor2010, Keating2004a} but at the cost of the overhead of the extra simulation.
One of the main contributions of this thesis is the proposal of a framework that uses channel flow as a precursor simulation for generating inflow for simulations with a TBL at the inflow.

This thesis is structured as follows.
Chapter~\ref{chap:model} describes the governing equations of fluid flow, introduces LES in a more formal way, and discusses the numerical algorithms used in the simulations which were performed as part of this work.
Chapter~\ref{chap:twoflows} gives a short summary of the physics of two wall-bounded flows that play a central role in this thesis, namely, the flat-plate TBL and fully-developed turbulent channel flow.
Chapter~\ref{chap:inflow} contains a detailed overview of the existing precursor-based methods for inflow generation and presents an inflow generation method based on a precursor channel flow simulation.
Chapter~\ref{chap:eddylicious} gives an overview of the architecture and functionality of a Python package for inflow generation, which was developed by the author.
Chapter~\ref{chap:results} contains results from numerical simulations of channel flow and of a TBL.
Finally, chapter~\ref{chap:concl} summarises the thesis and speculates regarding possible future work.

