\chapter{Eddylicious: A Python Package for Inflow Generation}
\label{chap:eddylicious}

This chapter is dedicated to the description of a Python package for turbulent inflow generation developed by the author.
The presented material is based on Paper~\rom{3}.
In the first section the purpose of the package is discussed.
This is followed by a section presenting the architecture of the code and then a section outlining the currently implemented functionality.

\section{Motivation and Purpose}
In the previous chapter an overview of the existing approaches towards inflow generation was presented.
Naturally, it is important to assess which methods perform best.
Several papers \cite{Keating2004a, Tabor2010, Pronk2012, Dietzel2014, Kanchi2013} investigate this, but each study typically considers  a relatively small number of methods.
One reason for this is that the inflow generation methods are usually implemented within a framework of a specific CFD solver.
This makes the task of testing a large set of methods tedious, since it involves a large amount of programming.

It is, however, possible to bypass this issue in the following way.
Most modern CFD solvers support reading boundary data from files stored on the hard-drive.
Therefore, it is possible to create a solver-independent inflow generation library, which would save the inflow fields to one or multiple files.
The task of making all the implemented methods available to a certain solver would then boil down to implementing the ability to save the fields in a file-format that the solver supports.

The success of this approach relies on a collaborative effort to fill up the developed code with various inflow generation techniques.
A research group with sufficient experience with a given method should thus be able to contribute the source code implementing this method to the library.
It is therefore imperative that the code of the library is open-source and that third-party contributions are welcome.

With these ideas in mind, the Python package \texttt{eddylicious} was created\footnote{https://github.com/timofeymukha/eddylicious}.
It is free software, provided under the GNU General Public Licence.
The currently implemented functionality of the package, as well as its principle components, are discussed further in this chapter.

One downside of the proposed framework is that it is impossible to implement methods that somehow interact with the flow-field inside the computational domain.
This includes weak recycling methods, such as the LWS method~\cite{Lund1998}.
Another example is the method developed in~\cite{Spille-Kohoff2001} which relies on introducing extra body-forces to the momentum equations.

Also, the size of the produced database of inflow velocities can potentially take up to several hundreds of gigabytes on the hard-drive.
The size is directly proportional to the density of the computational mesh at the inlet and the number of time-steps for which the velocities are being generated.

Nevertheless, \texttt{eddylicious} has the potential to grow into a comprehensive library of inflow generation methods, compatible with several CFD solvers.
Since \texttt{eddylicious} is written in Python, developers have access to a plethora of excellent libraries for scientific computing.
This allows rapid implementation of existing methods or prototyping new ones.
Furthermore, the programming is simplified because of the fact that such issues as geometry decomposition and similar solver-related difficulties do not have to be considered.

\section{Architecture of the Package}
In order to fulfil its purpose, the architecture of \texttt{eddylicious} has to allow for easy accommodation of new inflow generation methods and input/output (I/O) functionality.
To this end, the package is decomposed into three separate modules: \texttt{readers}, \texttt{writers}, and \texttt{generators}.

The \texttt{generators} module contains the various inflow generation methods.
The \texttt{writers} module implements the output of the generated velocities to different file-formats.
Finally, the \texttt{readers} module consists of functions for reading previously saved velocity fields from the hard-drive.
This functionality is needed for those methods that use a precursor simulation.

It is important that all the present generators support all the output formats that are in the \texttt{writers} module and, where it is needed, all the input formats from the \texttt{readers} module.
A common interface for all readers and writers has to be defined in order for this to work smoothly as the package grows.
Currently, however, the interface is not yet defined since the amount of I/O methods is still quite small.

The three modules described above are well suited to build up a library of inflow generation methods and associated I/O.
However, it is also necessary to provide the users a convenient way of using the library.
This is done with the help of executable Python scripts that accept user input via configuration files.
Each method in the \texttt{generators} module has an associated script.
The user can then configure what I/O formats should be used as well as provide any other input that the generation method requires.
Besides for the scripts associated with the inflow generators, the package also provides some utility-scripts that simplify certain routine tasks.

It is anticipated that the potential contributors to the package are not professional programmers, but rather researches and other CFD professionals.
Therefore, it is written using procedural programming.
More advanced paradigms such as object-oriented programming were avoided. 
 
\section{Implemented Functionality}

Currently the package includes several I/O formats and a single inflow generation method.
All of them assume that the only quantity that has to be generated is the velocity field.

An important limitation is the way the geometry of the inlet is currently represented inside the code.
It is currently assumed that the inlet is a rectangle aligned with the $y$-$z$ plane, and that it is meshed using a structured rectilinear grid.
This allows storing velocity fields as two-dimensional arrays, and  the row and column numbers to be used  to determine the geometrical position. 
Each row corresponds to a certain $y$ and each column to a certain $z$.
This type of inlet geometry is common for a range of canonical turbulent flows, but limits the usability of \texttt{eddylicious} for industrial applications.

Two output file-formats are supported.
The first one is a format supported natively by \OF{} .
The velocity fields are kept in a readable text file, essentially as an ordered list of values. 
A single file is output per time-step.
This is a major downside of the format, because hundreds of thousands  of files may have to be created, depending on the time frame of the simulation.
Such a big amount of files is difficult to handle.
A file containing a list of spatial coordinates is also saved, it contains the centres of the  faces forming the inlet boundary.

\texttt{Eddylicious} also supports outputting the generated fields to a single HDF5 file.
HDF stands for Hierarchical Data Format.
It was developed specifically for storing large amounts of scientific data.
The format supports defining named datasets which can, if needed, be classified into groups.
So called attributes can also be defined, which serve as meta-data for the datasets stored in the file.
Perhaps most importantly, HDF5 supports parallel I/O via the Message Passing Interface (MPI) which makes it well-suited for CFD solvers which always employ some way of distributing the solution process across several processors.
The ability to read boundary data from an HDF5 file was added to \OF{} since it is not natively supported\footnote{https://bitbucket.org/lesituu/timevaryingmappedhdf5fixedvalue}.

The currently implemented input functionality is similar.
In \OF{} one can choose to save sampled plane-data into several different file formats.
One of them, called \texttt{foamFile}, is supported by \texttt{eddylicious}.
\OF{} saves the sampled data to a separate file at each time-step. 
Therefore the problem of the huge amount of created files is present here as well.

The remedy is, again, using HDF5.
Instead of adding support for HDF5-output to \OF{}, a utility for converting a precursor database saved as multiple \texttt{foamFile}-formatted files to a single HDF5 file was created.
The produced file can then be used as input to \texttt{eddylicious}.

The implemented inflow generation method is now presented.
It is based on  the rescaling procedure from the LWS method~\cite{Lund1998}.
As mentioned above, weak recycling methods can't be directly implemented in \texttt{eddylicious} because the fields are generated prior to the execution of the simulation.
However, the rescaling used in~\cite{Lund1998} can be seen as a general way of transforming a given database of velocity fields sampled from a precursor TBL or channel flow simulation to match certain desired integral characteristics.

Also, note that if the properties of the inflow are chosen to be matched to those of the precursor,  the rescaling boils down to simply using the precursor velocity fields as inflow without any modification.
In other words, no rescaling is actually performed.
This allows to use the implemented generation method for the procedure described in section~\ref{sec:ourmethod}
 
Consider a number of  possible usage patterns.
\begin{itemize}

\item A precursor channel flow simulation at a given $\rey_\tau$ is performed and used to generate a database of velocity fields.
A simulation of a plane-diffuser is performed, using the precursor fields as inflow.
This is achieved by setting the parameters of the inflow field to match those of the precursor in the configuration file.

Alternatively, the precursor fields can be rescaled to a different $\rey_\tau$ using the rescaling defined by the LWS method thus allowing a single precursor to be used for several diffuser simulations (at the expense of a longer adaption length).
Note, that the grids in the precursor and in the inlet of the diffuser simulation don't have to match; spatial interpolation will automatically be employed.

\item The workflow defined in section~\ref{sec:ourmethod} for generating inflow for a TBL simulation is straightforward to apply.

\item A TBL simulation can be used to sample velocity fields at a station corresponding to a certain $\rey_\theta$.

A backward-facing step (BFS) simulation, for instance, can use the sampled fields as inflow.
However, it may occur that the same $\rey_\theta$ for the BFS is achieved via a different combination of dimensional parameters.
In this case the rescaling procedure in~\cite{Lund1998} can be employed by \texttt{eddylicious} to transform the inflow fields to a new set of dimensional parameters but preserving $\rey_\theta$.
\end{itemize}

This is not an exhausting list of scenarios, but these three cases provide a good illustration of the possibilities the inflow generation method gives.
The reader interested in learning how to configure and use \texttt{eddylicious} is referred to the online documentation\footnote{http://eddylicious.readthedocs.io/}.
It contains a user guide with an exhaustive description of all the existing functionality and guidelines for how to use the package.
A tutorial for using \texttt{eddylicious} with \OF{} is also provided.



 



  


