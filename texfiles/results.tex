\chapter{Numerical Experiments}
\label{chap:results}

In this chapter, several numerical experiments performed using \OF{} will be considered.
The first section presents the results from two studies on LES of fully-developed turbulent channel flow. 
One analyses the performance of several SGS models, and the other how the accuracy of the computed profiles is influenced by the resolution of the computational mesh.
In the second section, results from a TBL simulation employing the inflow generation method proposed in section~\ref{sec:ourmethod} are given.
Also, results from a simulation with inflow generated using the rescaling procedure defined in~\cite{Lund1998} are presented.
The adaption lengths associated with both methods are analysed.

\section{Turbulent Channel Flow}

Here, different aspects of simulating turbulent channel flow using LES are discussed and results from associated simulations performed in \OF{} are presented.
In particular, the meshing strategy and choice of the SGS model are addressed in detail.
The conclusions should be applicable to any solver employing discretization practices similar to \OF{}.
Also, it is anticipated that the guidelines developed for channel flow can be used as a starting point for simulations of other wall-bounded flows.


Paper \rom{1} is a technical report that presents the results from channel flow simulations at $\rey_\tau \approx 395$.
The motivation behind the performed work was to find what level of accuracy one could expect from an LES of a wall-bounded flow performed in \OF{} and evaluate roughly how it depends on the grid-size.
A natural step at that point was to base most modelling choices on a tutorial for LES of channel flow that is shipped with \OF{}.
A grid refinement study using three grids was performed.
Additionally, the methodology for computing higher order moments in \OF{} was developed which allowed to compute such quantities as skewness and flatness.
Two-point correlations in space were also computed which allowed to determine the integral length-scales of the flow.

The findings and developments recorded in Paper \rom{1} served as a base for further investigation of wall-bounded flows in our research group. In the following two years additional studies were performed regarding several modelling parameters, which allowed to further improve the methodology for using \OF{} for LES.
Results from these studies, to date not published elsewhere, are presented below.

In section~\ref{sec:numerics} it was pointed out that a major complication when it comes to the choice of modelling parameters in LES is the coupling between the filtering and the local size of the computational cell.

The choice of the cell-size and its distribution is directly connected to the size of the turbulent structures that will be resolved in the LES.
This, in turn, dictates how ``active'' the chosen SGS model will be and therefore its effect on the resulting pressure and velocity distributions.

On the other hand, as with any numerical simulation, the cell-size controls the magnitude of the numerical errors.
In particular, numerical dissipation.
Combined with the fact that SGS models based on the Boussinesq approximation introduce extra dissipation as well, it becomes extremely complicated to decide what combination of numerical schemes, SGS modelling and grid resolution to choose.
Naturally, the available computational resources are yet another aspect to consider.

This interdependency between various simulation choices makes it hard to analyse the effect of a single modelling parameter.
In the simulations presented here the numerical schemes are chosen to maintain second-order accuracy, as discussed in~\ref{sec:numerics}.
This can be considered standard for general-purpose CFD solvers and is therefore something that can be kept fixed.
This leaves grid resolution and SGS model choice as the two main modelling aspects that have to be considered.
To this end, two studies were performed.
In the first study, explicit SGS modelling is not employed at all (i.e. ILES was used), and different grid resolutions are tested.
In the second study, the grid resolution is fixed, and various SGS models are considered.
In both studies channel flow at $\rey_\tau \approx 550$ is simulated.
DNS data from \cite{LM:1} is used for reference in the figures with results below.

\subsection{Grid Resolution Study}
The choice of the cell-size distribution is closely connected to the physics of wall-bounded flows presented briefly in chapter~\ref{chap:twoflows}.
In the outer layer the relevant length-scale is the channel half-height $\delta$.
This region does not contain steep gradients and can therefore be discretized with cells of size $\approx\delta/25$,~\cite{Spalart1997}.
In the inner layer the relevant length-scale is $\delta_\nu \ll \delta$.
The flow physics in this near-wall region is complex and is not fully understood to date.
What is known, however, is that the region is populated by complex interacting coherent structures~\cite{Pope2000}.
In particular, streaks of relatively slowly moving fluid are observed for $y^+ < 40$, see figure~\ref{fig:streaks}.
These streaks can be $500$-$1000\delta_\nu$ long and the spacing between them is $80$-$120\delta_\nu$~\cite{Pope2000}.

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{figures/streaks.png}
	\caption{Snapshot of the velocity magnitude, scaled with the friction velocity $u_\tau$, from an LES of channel flow, $x$-$z$ cut-plane located at $y^+ \approx 20$. Streaks of slow-moving fluid are visible.}
	\label{fig:streaks}
\end{figure}

The streaks interact with the flow above in a manner that leads to a high production of turbulence in this region.
This means that, in order to correctly model the flow, this region has to be well-resolved.

All the meshes used in the study are structured and hexahedral.
Three combinations of span- and streamwise cell-sizes are considered. 
\begin{itemize}
\item \mbox{$\Delta x^+ = 50$, $\Delta z^+ = 20$, $N_\text{cells} \approx 1.7$ million} --- the coarsest considered grid.
\item \mbox{$\Delta x^+ = 50$, $\Delta z^+ = 10$, $N_\text{cells} \approx 3.4$ million} --- investigates the effect of refining in the spanwise direction.
\item \mbox{$\Delta x^+ = 25$, $\Delta z^+ = 20$, $N_\text{cells} \approx 3.4$ million} --- investigates the effect of refining in the streamwise direction.
\item \mbox{$\Delta x^+ = 25$, $\Delta z^+ = 10$, $N_\text{cells} \approx 6.8$ million} --- the finest grid considered, further refinement would lead to the simulation being unaffordable.
\end{itemize}

For every simulation $\Delta x^+ $ and $\Delta z^+$ is kept constant throughout the domain.
In the wall-normal direction the cell-size varies.
In chapter 13 of~\cite{Pope2000}, Pope proposes the following parametrized expression for the distribution of the cell-height.

\begin{equation}
	\label{eq:cellheight}
	\Delta y = \min\left[\max \left(\frac{y^+_1}{\rey_\tau}, \eta_\text{in}\eta \right), \frac{1}{y^+_\text{out}}\right]\delta,
\end{equation}
where $y^+_1$, $\eta_\text{in}$ and $y^+_\text{out}$ are constants and $\eta = y/\delta$.
Pope does not suggest any values for the constant parameters nor their physical meaning.
The following interpretation and choice of values was obtained by the author from Saleh Rezaeiravesh through personal communication.
\begin{itemize}
	\item $y^+_1$ --- this is the location of the first cell-centre scaled with $\delta_\nu$.
	The value of 1 is chosen in order to resolve the near-wall physics properly.
	
	\item $\eta_\text{in}$ --- the location of the end of the inner layer scaled with $\delta$.
	This is a Re-number dependent quantity, but the value of $0.1$ gives good results for a wide range of cases~\cite{Osterlund1999}.
	\item $1/y^+_\text{out}$ --- the location of the beginning of the outer layer scaled with $\delta_\nu$.
	This is also Re-dependent, however, the value of 30 works across a wide range of Re \cite{Pope2000}.
\end{itemize}

Equation~\eqref{eq:cellheight} implies the following cell-size distribution.
Up to the overlap region the cell-size is constant and equal to ${y^+_1}/{(\rey_\tau \delta)}$.
Then the size gradually grows until the value of $\delta/y^+_\text{out}$ is reached, which is used up to the centreline of the channel.
The distribution above $y=\delta$ mirrors the one below it.
The typical number of cells across the whole channel height is 100-150, depending on $\rey_\tau$.
For $\rey_\tau = 550$, the number is 104.

\begin{figure}
	\centering
	\includegraphics[scale=1]{figures/channel_umean.pdf}
	\caption{Mean streamwise velocity for different span- and streamwise mesh resolutions. In outer coordinates (left), in inner coordinates (right).}
	\label{fig:channel_umean}
\end{figure}

Figure~\ref{fig:channel_umean} shows the mean streamwise velocity in inner and outer coordinates.
In the outer coordinates, it is clear that the results are highly dependent on the streamwise resolution.
Both curves corresponding to $\Delta x^+=25$ show excellent agreement with DNS data, while $\Delta x^+=50$ results in a flatter profile.
When the data are represented in inner coordinates it is easy to observe that simulations with $\Delta z^+=10$ perform very well in the inner layer, while for $\Delta z^+=20$ the values get over-predicted in the overlap region.
In the outer layer, it is clear that $\Delta x^+=50$ is insufficient to capture the correct form of the profile, as was already observed in the plot in outer coordinates.
It is possible to conclude that all the considered grids give a reasonably good agreement with DNS for the mean velocity profile.
While improving the resolution along a single axis improves the quality of the solution, it is necessary to go down to $\Delta x^+ = 25$, $\Delta z^+ = 10$ in order to get results that are almost indistinguishable from DNS.

\begin{figure}[h!]
	\centering
	\includegraphics[scale=1]{figures/channel_rey1.pdf}
	\caption{The components of the Reynolds stress tensor for different span- and streamwise mesh resolutions.}
	\label{fig:channel_rey}
\end{figure}

Figure~\ref{fig:channel_rey} shows the non-zero components of the Reynolds stress tensor.
The streamwise component gets over-predicted on all meshes besides the finest one.
The coarsest mesh gives the largest over-prediction which is not surprising.

The behaviour for the wall-normal and spanwise components is similar.
The meshes with $\Delta z^+=10$ perform well, whereas using a coarser spanwise resolution leads to a larger under-prediction.
The turbulent shear stress is very well predicted using all meshes.

It is possible to conclude that decreasing $\Delta x^+$ while keeping the same $\Delta z^+$ has very little effect on all the Reynolds stresses, with the exception of the streamwise component.
There, refining from $\Delta x^+ = 50$ to 25, while keeping $\Delta z^+ = 10$ gives a big boost in accuracy.
On the other hand, a similar refinement but with $\Delta z^+$ kept at 20 does not lead to an equally large improvement.

The overall conclusion of the study is that using $\Delta x^+ = 25$, $\Delta z^+ = 10$ along with the used cell-size distribution in the wall normal direction leads to very accurate results for both first- and second-order statistics.
If coarsening is necessary, it is better to coarsen $\Delta x^+$ rather then $\Delta z^+$, as the latter has a larger effect on the accuracy in the inner layer.
However, a small $\Delta x^+$ does lead to a better prediction in the outer layer for the mean velocity profile.

\subsection{Subgrid Stress Model Study}
Here, a study analysing the performance of several SGS models is presented.
For that purpose the grid resolution is fixed, namely $\Delta x^+ = 50$ and $\Delta z^+ = 20$ will be used for the cell-size in the stream- and spanwise directions respectively.
In the wall-normal direction the distribution~\eqref{eq:cellheight} will be used.
This rather coarse mesh is chosen so that the SGS modelling has a significant effect on the results.
In the previous section it was shown that using no SGS model with this resolution leads to a profile which over-predicts the mean velocity in the inner layer and under-predicts it in the core of the channel.
Possibly, an SGS model can improve on that result.

The following models are considered in the study (see section~\ref{sec:sgs} for definitions).
\begin{itemize}
	\item ILES, i.e. no SGS model at all.
	\item The Smagorinsky model, see~\eqref{eq:smagorinsky}.
	\item The one-equation model, see~\eqref{eq:oneqeddy}.
	\item The one-equation model with near-wall damping using the van Driest damping function~\eqref{eq:vandriest}.
	\item A dynamic version of the one-equation model.
\end{itemize}

It is important to note that the models were not tuned in any way with respect to the values of the constants that they depend on.
Default values suggested in \OF{} were used.

Figure~\ref{fig:sgs_umean} shows the mean streamwise velocity profiles obtained with the considered models.
The difference in the results is more easily observed in the inner coordinates.
The ILES and the dynamic model perform very similarly to each other.
The one-equation and the Smagorinsky model also perform equally and significantly worse than the other models.
However, the large discrepancy seen in the plot in inner coordinates is mostly due to the over-predicted value of the friction velocity.
The shape of the profile is not that significantly different from those produced by ILES and the dynamic models.
Applying a damping function improves the performance of the one-equation model significantly.
It is hard to judge whether it performs better or worse than ILES, the overall error in the profile is of similar magnitude, but its distribution is different.

\begin{figure}[h!]
	\centering
	\includegraphics[scale=1]{figures/sgs_uy.pdf}
	\caption{Mean streamwise velocity for different SGS models. In outer coordinates (left), in inner coordinates (right).}
	\label{fig:sgs_umean}
\end{figure}

Figure~\ref{fig:sgs_rey} shows the computed non-zero components of the Reynolds stress tensor.
The observed trends are mostly similar to that exhibited by the mean velocity profile.
The dynamic model performs similar to the ILES.
The Smagorinsky and one-equation models perform significantly worse.
Interestingly, the one-equation model with damping only differs in performance from the undamped version in the accuracy of the $\langle u'u'\rangle$ profile.
There, the damping slightly improves the result; in fact, the model gives the best prediction of this component among all the considered models.

\begin{figure}[h!]
	\centering
	\includegraphics[scale=1]{figures/sgs_rey.pdf}
	\caption{The components of the Reynolds stress tensor for different SGS models.}
	\label{fig:sgs_rey}
\end{figure}

The conclusion that can be drawn from this study is that the considered SGS models, at least as they are implemented in \OF{}, do not offer significant improvement over not using any model at all and relying on numerical dissipation.
Only the dynamic one-equation model performed on par with ILES, but no improvement that would justify the associated increase in simulation time was observed.

While this study is too limited to draw any general conclusions regarding SGS modelling, it can serve as a guideline for users of \OF{} and other solvers employing similar numerical techniques.
It shows that simply not using an SGS model can be a justifiable decision and a possibility that is worth investigating.

\section{Turbulent Boundary Layer}
In this section, results from a ZPG-TBL simulation using the inflow generation method proposed in section~\ref{sec:ourmethod} is presented.
For comparison, results from a simulation which uses an approach based on the rescaling procedure proposed by Lund et al in~\cite{Lund1998} will also be shown.
The material presented here is essentially a short summary of Paper \rom{2}, therefore a more detailed description of the study can be found there.

In order to generate the inflow, a channel flow simulation needs to be conducted.
To this end, a simulation for $\rey_\tau \approx 550$ was performed using a grid with $\Delta x^+ =50$, $\Delta z+=20$ and~\eqref{eq:cellheight} for the cell-size distribution in the wall-normal direction.
No SGS model was employed.
The results of the simulation are essentially identical to what has been presented in the grid resolution study above for the corresponding grid.
Velocity values where sampled from a plane perpendicular to the streamwise direction and \texttt{eddylicious} was used to create a database of inflow fields taken from the bottom half of the sampling plane.

In the TBL simulation, a resolution close to that in the precursor channel flow simulation was used in the stream- and spanwise directions.
In the wall-normal direction the same distribution as in precursor was used up to $y=\delta$.
The cell-size was then maintained unchanged for another interval of size $\delta$ and then rapidly increased towards the top of the domain.
Again, no SGS modelling was employed.

The domain size for the TBL simulation was chosen to be large enough to nullify the effect of the boundary conditions on the top and sides of the domain.
The chosen length resulted in a simulation covering a range of $\rey_\theta$ from $\approx 835$ to $\approx 2400$.

\begin{figure}[h]
	\centering
	\includegraphics[scale=1]{figures/sim_schematic.pdf}
	\caption{Schematic showing the performed simulations and the associated inflow generation procedures.}
	\label{fig:sim_schematic}	
\end{figure}

Additionally, the conducted TBL simulation was used to sample velocity values at a location sufficiently far downstream of the inlet ($\rey_\theta \approx 1300$).
The sampled values were then rescaled with \texttt{eddylicious} using the rescaling procedure defined by the LWS method~\cite{Lund1998} to approximately match the $\rey_\theta$ at the inflow of the first TBL simulation.
The obtained values were then used as inflow for the second TBL simulation, see figure~\ref{fig:sim_schematic}.
This set-up mimics the weak recycling method defined in~\cite{Lund1998}, however, instead of obtaining the inflow values dynamically, they are computed using a database from a precursor TBL simulation.
Apart from the inflow generation method the set-up of the second TBL simulation is identical to that of the first one.

Figure~\ref{fig:cfh} shows the evolution of the friction coefficient, $c_f$, and the shape factor, $H=\delta^*/\theta$, as a function of $\rey_\theta$.
For $c_f$ it is clear that the adaption length for the proposed method is very short, while the rescaling-based inflow generation leads to a dip in $c_f$ before recovery happens.
For the shape factor, the overall agreement with DNS is worse for both methods.
The under-prediction of $H$ can, however, be anticipated based on the results for channel flow shown in the previous section.
Indeed, for the chosen grid resolution the produced mean velocity profiles were flatter than the DNS data, which corresponds to an under-predicted shape factor.
Regarding the adaption length, it is clear that for the proposed method the behaviour of $H$ stabilises faster.
The curve corresponding to the LWS rescaling exhibits a steep decline over a significant part of the domain, which is not consistent with the behaviour of the DNS data.


\begin{figure}[ht!]
	\centering
	\includegraphics[width=\linewidth]{figures/main_cf_h.png}
	\caption{Evolution of the friction coefficient, $c_f$, and the shape factor, $H$, as a function of $\rey_\theta$.}	
	\label{fig:cfh}
\end{figure}

Figure~\ref{fig:tbl_umean} shows the mean streamwise velocity at three $\rey_\theta$ for which DNS data are provided in~\cite{Schlatter2010}.
The overall accuracy of the profiles is in line with what could be expected based on the grid resolution study for channel flow.
An important result is that at $\rey_\theta \approx 1420$ the profiles corresponding to the two inflow generation methods are already in excellent agreement with each other.
Noticeably, in the inner layer the agreement is fairly good even at $\rey_\theta \approx 1000$, which reflects the fact that channel flow and the TBL have a similar flow structure in that region.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=\linewidth]{figures/main_u.png}
	\caption{Profiles of the streamwise component of velocity at $\rey_\theta \approx 1000$, $\rey_\theta \approx 1420$, $\rey_\theta \approx 2000$. The profiles are shifted by $\langle u\rangle/u_\tau^+=5$ upwards for increasing $\rey_\theta$.}
	\label{fig:tbl_umean}
\end{figure}

The overall conclusion is that the correct flow behaviour is recovered at $\rey_\theta$ somewhere in the range 1200-1300.
The corresponding range in terms of the momentum thickness of the inflow TBL is \mbox{$270\theta_\text{in}$-$370\theta_\text{in}$}.
For the rescaling method, the adaption length is of comparable size.
Based on these results it is possible to conclude that the performance of the proposed inflow generation method is on par with other precursor-based approaches.
Combined with its simplicity and robustness, this makes it an attractive alternative to methods based on weak recycling.