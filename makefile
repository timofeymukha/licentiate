file=lic

all:
	pdflatex $(file)
	bibtex $(file)
	pdflatex $(file)
	pdflatex $(file)

#pdf: all
#	dvipdfmx $(file)

clean:
	cleanTex.sh
